package AnnotationDataTest;


import java.lang.String;
import org.omg.CORBA.ExceptionList;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class DataProviderTest {

    @DataProvider(name = "Good")
    public Object[][] simpleDataProvider() {
        return new Object[][]{

                {"Thy edge should blunter be than appetite,"},
                {"They did conclude to bear dead Lucrece thence;"},
                {"To show her bleeding body thorough Rome,"},
                {"And so to publish Tarquin’s foul offence:"},
                {"Which"}
        };
    }

    @Test(dataProvider = "Good", dataProviderClass = DataProviderTest.class)
    public void testPass(String name) {

        try {
            Assert.assertTrue(name.length() > 10);
            System.out.println("Great!");
        }
        catch (AssertionError ex){
            ex.printStackTrace();
            System.out.println("Not great");
        }
    }
}

















